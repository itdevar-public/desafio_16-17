# Aplicacion: CV-rails
Es una aplicacion sencilla en donde mostramos un Curriculum Vitae. La aplicacion es creada con Ruby 3.0.2.

## Modo de uso rapido

`cd /vagrant` Ir a la carpeta donde se encuenta la aplicacion.

`rails s -b 0.0.0.0` Iniciamos la aplicacion

`localhost:3000` Accedemos a la aplicacion desde nuestro navegador
