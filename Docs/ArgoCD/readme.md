# Archivos YML para ArgoCD
Estos archivos son la configuracion para nuestra aplicacion sin usar Helm.

## Deployment.yml
Configura el Deployment de la aplicacion: nombre, cantidad de pods, replicas, etc .

## Service.yml
Crea y configura el servicio con la configuracion indicada.

## Application.yml
Configura la Aplicacion: nombre, origen Git y destino del clúster.

## Modo de Uso

`cd /vagrant/argoCD` Vamos a la carpeta donde estan las configuraciones.

`argocd app create rails-app --repo https://gitlab.com/itdevar-public/desafio_16-17.git --path argoCD --dest-server https://kubernetes.default.svc --dest-namespace rails-app` Creamos la aplicacion.

`Ir a la API de ArgoCD y sincronizar los recursos`

