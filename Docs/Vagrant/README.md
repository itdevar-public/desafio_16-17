# Maquina
Las configuraciones de Hardware de la maquina virtual se encuentran en Vagrantfile

# Configuraciones extra
Las configuraciones extra e instalacion de las herramientas, se encuentran en el archivo "bootstrap.sh"

# Pasos de ejecucion maquina virtual

`vagrant up`

`vagrant ssh`
