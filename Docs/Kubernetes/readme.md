# Archivos YML
Estos archivos son la configuracion para conventir recursos en Kubernetes.

## Deployment.yml
Configura el Deployment de la aplicacion: nombre, cantidad de pods, replicas, etc.

## Modo de Uso

`kubectl apply -f deployment.yml` Creamos el deployment. 

`kubectl expose deployment rails-app --type=NodePort --port=3000` Creamos el servicio y exponemos los puertos.

`minikube service rails-app --url` Creamos un tunel para conectarnos fuera del clúster de Kubernetes.

`curl http://192.168.49.2:31906` Verificamos la aplicacion.

