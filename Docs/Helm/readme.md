# Archivos Helm
Estos archivos son la configuracion para nuestra aplicacion usando Helm.

## template/deployment.yaml
Configura el Deployment de la aplicacion: nombre, cantidad de pods, replicas, etc .

## template/service.yaml
Crea y configura el servicio con la configuracion indicada.

## Chart.yaml
Informacion acerca de la aplicacion

## values.yaml
Configuramos el valor de las variables.

## Modo de Uso
`cd /vagrant` Vamos a la carpeta donde estan las configuraciones.
`helm install rails-app-helm rails rails.app/` Instalamos el paquete.
`minikube tunnel` Creamos un tunel para conectarnos fuera del clúster.
`curl <IP_MINIKUBE:NODEPORT>` Verificamos la aplicacion.
