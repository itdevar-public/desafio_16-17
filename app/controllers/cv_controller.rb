class CvController < ApplicationController

  # Muestra el CV basado en datos de un archivo YAML
  def show
    cv_path = Rails.root.join('data', 'cv.yml') # Asegúrate de que el archivo YAML esté en la carpeta 'data' dentro de 'rails_root'
    yaml_data = YAML.load_file(cv_path)

    # Asignar variables de instancia para la vista
    @general = yaml_data['general']
    @skills = yaml_data['skills']
    @other_skills = yaml_data['other_skills']
    @jobs = yaml_data['jobs']
    @education = yaml_data['education']
    @projects = yaml_data['projects']
  end
end
